﻿using M120_first_WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lottozahlen.Models
{
    public class Draw : ModelBase
    {
        private int _count;
        public int Count
        {
            get => _count;

            set
            {
                _count = value;

                OnPropertyChanged("Count");
            }
        }

        private int _minNumber;
        public int MinNumber
        {
            get => _minNumber;

            set
            {
                _minNumber = value;

                OnPropertyChanged("MinNumber");
            }
        }

        private int _maxNumber;
        public int MaxNumber
        {
            get => _maxNumber;

            set
            {
                _maxNumber = value;

                OnPropertyChanged("MaxNumber");
            }
        }

        public Draw()
        {
            Count = 0;
            MinNumber = 0;
            MaxNumber = 0;
        }


        public static int[] DrawLottery(int count, int min, int max)
        {
            int[] numbers = new int [count];

            Random rd = new Random();
            for (int i = 0; i < count; i++)
            {
                int number = rd.Next(min, max);
                while (numbers.Contains(number))
                {
                    number = rd.Next(min, max);
                }
                numbers[i] = number;
            }

            return numbers;
        }
    }
}
