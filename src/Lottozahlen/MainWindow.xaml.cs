﻿using Lottozahlen.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lottozahlen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Draw draw = new Draw();

        public MainWindow()
        {
            DataContext = draw;
            InitializeComponent();
        }


        private void Button_Draw(object sender, RoutedEventArgs e)
        {
            lotteryPanel.Children.Clear();
            int[] draws = Draw.DrawLottery(draw.Count, draw.MinNumber, draw.MaxNumber);
            
            for (int i = 0; i < draw.Count; i++)
            {
                Label dynamicLabel = new Label();

                dynamicLabel.Content = $"Number {i + 1}: " + draws[i];

                lotteryPanel.Children.Add(dynamicLabel);
            }
        }
    }
}
