﻿using Commands.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commands.ViewModels
{
    public class ViewModel
    {
        public SimpleCommand SimpleCommand { get; set; }

        public ViewModel()
        {
            SimpleCommand = new SimpleCommand(this);
        }

        public void SimpleMethod()
        {
            Debug.WriteLine("Hello");
        }
    }
}
