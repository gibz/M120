﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Commands.ViewModels.Commands
{
    public class SimpleCommand : ICommand
    {
        public ViewModel ViewModel { get; set; }

        public SimpleCommand(ViewModel viewModel)
        {
            ViewModel = viewModel;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
            //throw new NotImplementedException();
        }

        public void Execute(object parameter)
        {
            ViewModel.SimpleMethod();
        }
    }
}
