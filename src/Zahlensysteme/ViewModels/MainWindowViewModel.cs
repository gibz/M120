﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zahlensysteme.Models;

namespace Zahlensysteme.ViewModels
{

    class MainWindowViewModel : ModelBase
    {
        public int _currentvalue { get; set; }
        public string Decimal {
            get { return Convert.ToString(_currentvalue); }
            set
            {
                _currentvalue = Convert.ToInt32(value);
                OnPropertyChanged("");
            }
        }
        public string Hexadecimal
        {
            get { return Convert.ToString(_currentvalue, 16); }
            set
            {
                _currentvalue = Convert.ToInt32(value);
                OnPropertyChanged("");

            }
        }
    }
}
