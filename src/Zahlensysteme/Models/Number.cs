﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zahlensysteme.Models
{
    public class Number : ModelBase
    {
        private int _dec;
        public int Dec
        {
            get => _dec;

            set
            {
                _dec = value;

                OnPropertyChanged("Dec");
            }
        }

        private int _bin;
        public int Bin
        {
            get => _bin;

            set
            {
                _bin = value;

                OnPropertyChanged("Bin");
            }
        }

        private int _oct;
        public int Oct
        {
            get => _oct;

            set
            {
                _oct = value;

                OnPropertyChanged("Oct");
            }
        }

        private int _hex;
        public int Hex
        {
            get => _hex;

            set
            {
                _hex = value;

                OnPropertyChanged("Hex");
            }
        }
    }
}
