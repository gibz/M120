﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using M120_first_WPF.Models;
using M120_first_WPF.Models.ViewModels;

namespace M120_first_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        public MainWindow()
        {
            InitializeComponent();
            ContactViewModel contactViewModel = new ContactViewModel();
            this.DataContext = contactViewModel;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(contact.Adresse.Ort);
        }

        private void ButtonBase2_OnClick(object sender, RoutedEventArgs e)
        {
            //contact.Lastname = "fff";
            //contact.Adresse.Ort = "Barr";
        }
    }
}
