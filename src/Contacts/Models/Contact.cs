﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M120_first_WPF.Models
{
  public  class Contact :ModelBase
    {
        private string _lastname;
      
        public string Lastname
        { 
            get => _lastname;

            set
            {
               
                _lastname = value;

                OnPropertyChanged("Lastname");
            }
        }

        private string _firstname;

        public string Firstname
        {
            get => _firstname;

            set
            {

                _firstname = value;

                OnPropertyChanged("Firstname");
            }
        }

        public Adresse Adresse { get; set; }


        public Contact(string firstname, string lastname)
        {
            Firstname = firstname;
            Lastname = lastname;
        }
    }
}
