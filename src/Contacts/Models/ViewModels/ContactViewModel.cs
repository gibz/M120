﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M120_first_WPF.Models.ViewModels
{
   public class ContactViewModel :ModelBase
   {
       private Contact _contact;

       public string Lastname
        {
            get => _contact.Lastname;

            set
            {

                _contact.Lastname = value;

                OnPropertyChanged("Lastname");
            }
        }

       public string Firstname
        {
            get => _contact.Firstname;

            set
            {

                _contact.Firstname = value;

                OnPropertyChanged("Firstname");
            }
        }

        public Adresse Adresse { get; set; }

        public int Plz
        {
            get => _contact.Adresse.Plz;

            set
            {

                _contact.Adresse.Plz = value;

                OnPropertyChanged("Plz");
            }
        }
        public string Ort
        {
            get => _contact.Adresse.Ort;

            set
            {

                _contact.Adresse.Ort = value;

                OnPropertyChanged("Ort");
            }
        }

        public ContactViewModel()
        {
          Contact  contact = new Contact("Muster", "Max");
            contact.Adresse = new Adresse() { Ort = "Zug", Plz = 8005 };
            _contact = contact;
        }
   }
}
