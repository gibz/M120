﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace M120_first_WPF.Models
{
    public class ModelBase : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {   //Checks if PropertyChanged is not null meaning something has changed
            if (PropertyChanged != null)
            {  
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

