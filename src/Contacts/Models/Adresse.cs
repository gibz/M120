﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M120_first_WPF.Models
{
  public  class Adresse : ModelBase
  {
      private int _plz;
        public int Plz { get => _plz;

            set
            {

                _plz = value;

                OnPropertyChanged("Plz");
            }
        }

        private string _ort;
        public string Ort { get => _ort; set
            {

                _ort = value;

                OnPropertyChanged("Ort");
            }
        }
    }
}
